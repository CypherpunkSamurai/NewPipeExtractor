/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.schabi.newpipe.extractor.services.xh.extractors;

import com.grack.nanojson.JsonArray;
import com.grack.nanojson.JsonObject;
import com.grack.nanojson.JsonParser;
import com.grack.nanojson.JsonParserException;
import edu.umd.cs.findbugs.annotations.NonNull;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.schabi.newpipe.extractor.InfoItem;
import org.schabi.newpipe.extractor.MetaInfo;
import org.schabi.newpipe.extractor.MultiInfoItemsCollector;
import org.schabi.newpipe.extractor.Page;
import org.schabi.newpipe.extractor.StreamingService;
import org.schabi.newpipe.extractor.downloader.Downloader;
import org.schabi.newpipe.extractor.exceptions.ExtractionException;
import org.schabi.newpipe.extractor.exceptions.ParsingException;
import org.schabi.newpipe.extractor.linkhandler.SearchQueryHandler;
import org.schabi.newpipe.extractor.search.SearchExtractor;
import org.schabi.newpipe.extractor.services.xh.XhService;
import static org.schabi.newpipe.extractor.services.xh.XhService.HOST;
import org.schabi.newpipe.extractor.services.xh.extractors.items.XhVideo;

/**
 *
 * @author Rakesh
 */
public class XhSearchExtractor extends SearchExtractor {
    
    public XhSearchExtractor(final StreamingService service, final SearchQueryHandler query) {
        super(service, query);
    }

    @NonNull
    @Override
    public String getSearchSuggestion() throws ParsingException {
        return "";
    }

    @Override
    public boolean isCorrectedSearch() throws ParsingException {
        return false;
    }

    @Override
    public List<MetaInfo> getMetaInfo() throws ParsingException {
        return Collections.emptyList();
    }

    @Override
    public InfoItemsPage<InfoItem> getInitialPage() throws IOException, ExtractionException {
        
        Page p = new Page(this.linkHandler.getUrl());
        return getPage(p);
        
    }

    @Override
    public InfoItemsPage<InfoItem> getPage(Page page) throws IOException, ExtractionException {
        // Collector
        final MultiInfoItemsCollector collector = new MultiInfoItemsCollector(getServiceId());
        
        // String
        String url = page.getUrl();

        // HTML
        Document content = Jsoup.connect(url).get();
        
        // JS
        final String js = content.getElementById("initials-script").html();

        // Parse
        String info_cdn = js.substring(0, js.length() - 1).replace("window.initials=", "");
        System.out.println(info_cdn);
        
        try {
            // parse
            final JsonObject info = JsonParser.object().from(info_cdn);
            
            // list
            final JsonArray list = info.getObject("searchResult").getArray("models");
            
            for (int i = 0; i < list.size(); i++) {
                
                final JsonObject v = list.getObject(i);
                collector.commit(new XhStreamInfoItemExtractor(
                        new XhVideo(
                                v.getInt("id"),
                                v.getString("title"),
                                "",
                                v.getInt("duration"),
                                v.getInt("created"),
                                v.getString("pageURL"),
                                v.getString("thumbURL"),
                                0,
                                "",
                                "",
                                "",
                                new HashMap<String, String>()
                        )
                ));
                // log
                System.out.println(v.getString("title"));
            }
            
            // split url
            int l = url.split("/").length;
            
            int currentPage = Integer.parseInt(
                  page.getUrl().split("/")[l - 1]
            );

            // if there is next page
            String nextPageUrl = "";
            if (currentPage < list.size()) {
                for(int i=0; i < l-1; i++){
                    nextPageUrl = nextPageUrl + page.getUrl().split("/")[i] + "/";
                }
                nextPageUrl = nextPageUrl + String.valueOf(currentPage + 1);
            }
            
            return new InfoItemsPage<>(collector, new Page(nextPageUrl));
        } catch (JsonParserException ex) {
            Logger.getLogger(XhSearchExtractor.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
        
    }

    @Override
    public void onFetchPage(Downloader downloader) throws IOException, ExtractionException {}
    
}
