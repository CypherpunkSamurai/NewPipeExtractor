/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.schabi.newpipe.extractor.services.xh.extractors;

import org.schabi.newpipe.extractor.exceptions.ParsingException;
import org.schabi.newpipe.extractor.localization.DateWrapper;
import org.schabi.newpipe.extractor.services.xh.extractors.items.XhVideo;
import org.schabi.newpipe.extractor.stream.StreamInfoItemExtractor;
import org.schabi.newpipe.extractor.stream.StreamType;

/**
 *
 * @author Rakesh
 */
public class XhStreamInfoItemExtractor implements StreamInfoItemExtractor {

    XhVideo video;

    public XhStreamInfoItemExtractor(XhVideo video) {
        this.video = video;
    }

    @Override
    public StreamType getStreamType() throws ParsingException {
        return StreamType.VIDEO_STREAM;
    }

    @Override
    public boolean isAd() throws ParsingException {
        return false;
    }

    @Override
    public long getDuration() throws ParsingException {
        return video.getDuration();
    }

    @Override
    public long getViewCount() throws ParsingException {
        return -1;
    }

    @Override
    public String getUploaderName() throws ParsingException {
        return video.getAuthorName();
    }

    @Override
    public String getUploaderUrl() throws ParsingException {
        return video.getAuthorPageURL();
    }

    @Override
    public String getUploaderAvatarUrl() throws ParsingException {
        return video.getAuthorThumb();
    }

    @Override
    public boolean isUploaderVerified() throws ParsingException {
        return false;
    }

    @Override
    public String getTextualUploadDate() throws ParsingException {
        return null;
    }

    @Override
    public DateWrapper getUploadDate() throws ParsingException {
        return null;
    }

    @Override
    public String getName() throws ParsingException {
        return this.video.getTitle();
    }

    @Override
    public String getUrl() throws ParsingException {
        return this.video.getPageURL();
    }

    @Override
    public String getThumbnailUrl() throws ParsingException {
        return this.video.getThumbURL();
    }

}
