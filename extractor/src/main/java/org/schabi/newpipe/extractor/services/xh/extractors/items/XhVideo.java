/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.schabi.newpipe.extractor.services.xh.extractors.items;

import com.grack.nanojson.JsonObject;
import com.grack.nanojson.JsonParser;
import com.grack.nanojson.JsonParserException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 *
 * @author Rakesh
 */
public class XhVideo {

    int id;

    String title;
    String description;

    int duration;
    int created;

    String pageURL;
    String thumbURL;

    int authorID;
    String authorName;
    String authorThumb;
    String authorPageURL;

    Map<String, String> sources = null;

    public XhVideo(int id, String title, String description, int duration, int created, String pageURL, String thumbURL, int authorID, String authorName, String authorThumb, String authorPageURL, HashMap<String, String> streams) {
        this.id = id;
        this.title = title;
        this.description = description;

        this.duration = duration;
        this.created = created;

        this.pageURL = pageURL;
        this.thumbURL = thumbURL;

        this.authorID = authorID;
        this.authorName = authorName;
        this.authorThumb = authorThumb;
        this.authorPageURL = authorPageURL;

        this.sources = streams;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public int getDuration() {
        return duration;
    }

    public int getCreated() {
        return created;
    }

    public String getPageURL() {
        return pageURL;
    }

    public String getThumbURL() {
        return thumbURL;
    }

    public int getAuthorID() {
        return authorID;
    }

    public String getAuthorName() {
        return authorName;
    }

    public String getAuthorThumb() {
        return authorThumb;
    }

    public String getAuthorPageURL() {
        return authorPageURL;
    }

    public Map<String, String> getSourceURLs() throws IOException, JsonParserException {
        /*
            Parse 
         */
        Document html = Jsoup.connect(this.pageURL).get();
        // Parse Content
        String content = html.selectFirst("#initials-script").html();
        String info_cdn = content.substring(0, content.length() - 1).replace("window.initials=", "");

        // Use NanoJSON to convert to VideoItem
        final JsonObject json = JsonParser.object().from(info_cdn);
        final JsonObject videoModel = json.getObject("videoModel");

        HashMap<String, String> streams = new HashMap<>();

        // iterate urls
        JsonObject mp4 = videoModel.getObject("sources").getObject("mp4");
        if (!mp4.isEmpty()) {
            final Object keys[] = mp4.keySet().toArray();
            for (int i = 0; i < keys.length; i++) {
                final String key = keys[i].toString();
                System.out.println(key);
            }
        }

        return streams;
    }

    public Map<String, String> getSources() {
        //        if (sources.isEmpty() || sources == null) {
        //            // check if pageurl is set
        //            if (!pageURL.isBlank() || !pageURL.isEmpty()) {
        //                // parse the page
        //                try {
        //                    this.sources = getSourceURLs();
        //                } catch (Exception e) {
        //                    e.printStackTrace();
        //                }
        //            }
        //        }
        return sources;
    }

}
