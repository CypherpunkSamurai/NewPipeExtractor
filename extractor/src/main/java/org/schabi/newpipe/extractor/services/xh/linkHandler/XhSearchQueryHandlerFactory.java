/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.schabi.newpipe.extractor.services.xh.linkHandler;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;
import java.util.logging.Logger;
import org.schabi.newpipe.extractor.exceptions.ParsingException;
import org.schabi.newpipe.extractor.linkhandler.SearchQueryHandlerFactory;
import static org.schabi.newpipe.extractor.services.xh.XhService.BASE_URL;

/**
 *
 * @author Rakesh
 */
public class XhSearchQueryHandlerFactory extends SearchQueryHandlerFactory {

    public static XhSearchQueryHandlerFactory getInstance() {
        return new XhSearchQueryHandlerFactory();
    }
    
    @Override
    public String getUrl(final String query,
            final List<String> contentFilter,
            final String sortFilter) throws ParsingException {
        try {
            
            // log
            System.out.println(BASE_URL + "/search/" + URLEncoder.encode(query, "UTF-8") + "/1");
            
            return BASE_URL + "/search/" + URLEncoder.encode(query, "UTF-8") + "/1";
        } catch (final UnsupportedEncodingException e) {
            throw new ParsingException("query \"" + query + "\" could not be encoded", e);
        }
    }
    
}
