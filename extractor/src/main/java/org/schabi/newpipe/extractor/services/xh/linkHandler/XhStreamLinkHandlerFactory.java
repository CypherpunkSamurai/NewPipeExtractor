/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.schabi.newpipe.extractor.services.xh.linkHandler;

import org.schabi.newpipe.extractor.exceptions.ParsingException;
import org.schabi.newpipe.extractor.linkhandler.LinkHandlerFactory;
import static org.schabi.newpipe.extractor.services.xh.XhService.HOST;
import org.schabi.newpipe.extractor.utils.Parser;

/**
 *
 * @author Rakesh
 */
public class XhStreamLinkHandlerFactory extends LinkHandlerFactory {
    
    private static final String PATTERN = "http?s\\:\\/\\/(xhamster?.+)\\/videos\\/([a-zA-Z0-9-_\\.]+)";
    
    @Override
    public String getId(String url) throws ParsingException {
        String streamId = null;
        try {
            streamId = Parser.matchGroup(PATTERN, url, 2);
        } catch (final Parser.RegexException ignored) {}
        
        // return id
        return streamId;
    }

    @Override
    public String getUrl(String id) throws ParsingException {
        return "https://" + HOST + "/videos/" + id;
    }

    @Override
    public boolean onAcceptUrl(String url) throws ParsingException {
        try {
            return getId(url) != null;
        } catch (final ParsingException e) {
            return false;
        }
    }
    
}
